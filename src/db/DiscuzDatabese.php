<?php
// Discuz数据库操作

namespace DiscuzDb\db;

use DB;

trait DiscuzDatabese
{
    private $discuzGetSql;
    
    private function discuz_stop($stop = 'dd')
    {
        if (!empty($stop)) {
            $this->discuzGetSql = $stop;
        }
    }
    
    private function discuz_fetch_first($sql, $arg = [])
    {
        if ($this->discuzGetSql) {
            return $this->discuz_stopExecute(DB::format($this->paramBind($sql, $arg), $arg));
        }
        return DB::fetch_first($this->paramBind($sql, $arg), $arg);
    }
    
    private function discuz_fetch_all($sql, $arg = [])
    {
        return DB::fetch_all($this->paramBind($sql, $arg), $arg);
    }
    
    private function discuz_result_first($sql, $arg = [])
    {
        return DB::result_first($this->paramBind($sql, $arg), $arg);
    }
    
    /**
     * 查询
     *
     * @return void
     */
    private function discuz_query($sql, $arg = [], $getAffectedRows = false)
    {
        if ($this->discuzGetSql) {
            return $this->discuz_stopExecute(DB::format($this->paramBind($sql, $arg), $arg));
        }
        
        $return = DB::query($this->paramBind($sql, $arg), $arg);
        if ($getAffectedRows) {
            return DB::affected_rows();
        }
        return $return;
    }
    
    private function discuz_insert(string $table, array $data = [], bool $getLastInsID = false)
    {
        return DB::insert($table, $data, $getLastInsID);
    }
    
    /**
     * 取最后插入的ID
     *
     * @return mixed
     */
    private function discuz_getLastInsID()
    {
        return DB::insert_id();
    }
    
    private function discuz_insert_sql(string $sql, bool $getLastInsID = false)
    {
        $return = DB::query($sql);
        return $getLastInsID ? DB::insert_id() : $return;
    }
    
    /**
     * @param string  $table
     * @param array   $data
     * @param integer $limit 每次写入数据限制
     * @return integer
     */
    private function discuz_insertAll(string $table, array $data = [], $limit = 0)
    {
        $this->discuz_startTrans();
        foreach ($data as $v) {
            DB::insert($table, $v);
        }
        $this->discuz_commit();
        
        // 返回sql语句所影响的记录行数
        return count($data);
    }
    
    private function discuz_replace(string $table, array $data = [], bool $getLastInsID = false)
    {
        if (DB::insert($table, $data, false, true)) {
            return $getLastInsID ? DB::insert_id() : true;
        }
        return false;
    }
    
    private function discuz_update($sql, $getAffectedRows = false)
    {
        $return = DB::query($sql);
        return $getAffectedRows ? DB::affected_rows() : $return;
    }
    
    private function discuz_update_array(string $table, array $data = [], $where = false)
    {
        if (!$where) {
            return false;
        }
        return DB::update($table, $data, $where);
    }
    
    private function discuz_update_handle_where($where, $table_name = '')
    {
        $temp = [];
        foreach ($where as $item) {
            foreach ($item as $v) {
                if (count($v) != 3) {
                    continue;
                } else if ($v[1] != '=') {
                    exit($table_name . ' insertMode 为 array时，where只支持等于号(暂不支持其他操作符)');
                }
                $temp[$v[0]] = $v[2];
            }
        }
        return $temp;
    }
    
    private function discuz_delete($sql, $getAffectedRows = false)
    {
        $return = DB::query($sql);
        return $getAffectedRows ? DB::affected_rows() : $return;
    }
    
    /**
     * 事务开始
     *
     * @return void
     */
    private function discuz_startTrans()
    {
        DB::query("SET AUTOCOMMIT=0");
        DB::query("BEGIN");
    }
    
    /**
     * 事务回滚
     *
     * @return void
     */
    private function discuz_rollback()
    {
        DB::query("ROLLBACK");
        DB::query("SET AUTOCOMMIT=1");
    }
    
    /**
     * 提交事务
     *
     * @return void
     */
    private function discuz_commit()
    {
        DB::query("COMMIT");
        DB::query("SET AUTOCOMMIT=1");
    }
    
    /**
     * 执行数据库Xa事务
     *
     * @access public
     * @param callable $callback 数据操作方法回调
     * @return mixed
     * @throws PDOException
     * @throws \Exception
     * @throws \Throwable
     */
    private function discuz_transactionXa(callable $callback)
    {
        $xid = uniqid('xa');
        $this->startTransXa($xid);
        try {
            $result = null;
            if (is_callable($callback)) {
                $result = $callback();
            }
            $this->prepareXa($xid);
            $this->commitXa($xid);
            return $result;
        } catch (\Exception|\Throwable $e) {
            $this->rollbackXa($xid);
            throw $e;
        }
    }
    
    /**
     * 启动XA事务
     *
     * @access public
     * @param string $xid XA事务id
     * @return void
     */
    private function discuz_startTransXa(string $xid)
    {
        DB::query("XA START '$xid'");
    }
    
    /**
     * 预编译XA事务
     *
     * @access public
     * @param string $xid XA事务id
     * @return void
     */
    private function discuz_prepareXa(string $xid)
    {
        DB::query("XA END '$xid'");
        DB::query("XA PREPARE '$xid'");
    }
    
    /**
     * 提交XA事务
     *
     * @access public
     * @param string $xid XA事务id
     * @return void
     */
    private function discuz_commitXa(string $xid)
    {
        DB::query("XA COMMIT '$xid'");
    }
    
    /**
     * 回滚XA事务
     *
     * @access public
     * @param string $xid XA事务id
     * @return void
     */
    private function discuz_rollbackXa(string $xid)
    {
        DB::query("XA ROLLBACK '$xid'");
    }
    
    /**
     * 获取返回或者影响的记录数
     *
     * @return integer
     */
    private function discuz_affected_rows()
    {
        return DB::affected_rows();
    }
    
    /**
     * 取指定表全部字段名
     *
     * @param string $table    表全名(带前缀)
     * @param bool   $detailed 是否获取详细字段信息
     * @return array
     */
    public function discuz_getFields($table, $detailed = false)
    {
        $list   = DB::fetch_all("select * from information_schema.COLUMNS where table_name = '{$table}'");
        $return = [];
        foreach ($list as $v) {
            if ($detailed) {
                $return[$v['COLUMN_NAME']] = [
                    'name'      => $v['COLUMN_NAME'],
                    'type'      => $v['COLUMN_TYPE'],
                    'data_type' => $v['DATA_TYPE'],
                    'notnull'   => $v['IS_NULLABLE'] == 'NO',
                    'default'   => $v['COLUMN_DEFAULT'],
                    'primary'   => $v['COLUMN_KEY'] == 'PRI',
                    'autoinc'   => $v['EXTRA'] == 'auto_increment',
                    'comment'   => $v['COLUMN_COMMENT'],
                ];
            } else {
                $return[] = $v['COLUMN_NAME'];
            }
        }
        return $return;
    }
    
    /**
     * 取指定表全部字段名+字段类型
     *
     * @param string $table   表全名(带前缀)
     * @param string $getType 获取类型 string|int
     * @return array
     */
    public function discuz_getFieldType($table, $getType = 'string')
    {
        $list   = $this->discuz_getFields($table, true);
        $return = [];
        if ($getType == 'string') {
            foreach ($list as $k => $v) {
                $return[$k] = $v['data_type'];
            }
            
        } else {
            $intType   = ['int', 'integer', 'tinyint', 'smallint', 'mediumint', 'bigint'];
            $floatType = ['float', 'double', 'decimal'];
            foreach ($list as $k => $v) {
                if (in_array($v['data_type'], $intType)) {
                    $return[$k] = 1;
                } else if (in_array($v['data_type'], $floatType)) {
                    $return[$k] = 21;
                } else {
                    $return[$k] = 2;
                }
            }
        }
        
        return $return;
    }
    
    /**
     * 解析参数绑定
     *
     * @param string $sql
     * @param array  $bind
     * @return mixed|void
     */
    private function paramBind($sql, &$bind = [])
    {
        if (!$bind) {
            return $sql;
        }
        $newSql = $sql;
        foreach ($bind as $key => $field) {
            // 类型1，如：Db::query("select * from user where id=? AND status=?", [8, 1]);
            if (is_numeric($key)) {
                $newSql = str_replace('=?', '=' . DB::quote($field, true), $newSql);
                
                // 类型2命名绑定，如：Db::query("update user set name=:name where status=:status", ['name' => 'php', 'status' => 1]);
            } else {
                if (stripos($newSql, '=:' . $key) !== false) {
                    unset($bind[$key]);
                }
                $newSql = str_replace('=:' . $key, '=' . DB::quote($field, true), $newSql);
            }
        }
        return $newSql;
    }
    
    /**
     * 停止执行
     *
     * @param string $sql 当前的SQL语句
     * @return mixed|void
     */
    private function discuz_stopExecute($sql)
    {
        if ($this->discuzGetSql === true) {
            return $sql;
        } else if (function_exists('dd')) {
            dd($sql);
        } else {
            print_r($sql);
            exit();
        }
    }
}
