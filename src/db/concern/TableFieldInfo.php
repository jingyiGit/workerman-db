<?php
declare (strict_types=1);

namespace DiscuzDb\db\concern;

/**
 * 数据字段信息
 */
trait TableFieldInfo
{
  
  /**
   * 获取数据表字段信息
   *
   * @access public
   * @param string $tableName 数据表名
   * @return array
   */
  public function getTableFields($tableName = ''): array
  {
    if ('' == $tableName) {
      $tableName = $this->getTable();
    }
    return $this->discuz_getFields($tableName);
  }
  
  /**
   * 获取详细字段类型信息
   *
   * @access public
   * @param string $tableName 数据表名称
   * @return array
   */
  public function getFields(string $tableName = ''): array
  {
    return $this->discuz_getFields($tableName ?: $this->getTable('',true));
  }
  
  /**
   * 获取字段类型信息
   *
   * @access public
   * @return array
   */
  public function getFieldsType(): array
  {
    if (!empty($this->options['field_type'])) {
      return $this->options['field_type'];
    }
    return $this->discuz_getFieldType($this->getTable('',true));
  }
  
  /**
   * 获取字段类型信息
   *
   * @access public
   * @param string $field 字段名
   * @return string|null
   */
  public function getFieldType(string $field)
  {
    $fieldType = $this->getFieldsType();
    
    return $fieldType[$field] ?? null;
  }
  
  /**
   * 获取字段类型信息
   *
   * @access public
   * @return array
   */
  public function getFieldsBindType(): array
  {
    return $this->discuz_getFieldType($this->getTable('',true),'int');
  }
  
  /**
   * 获取字段类型信息
   *
   * @access public
   * @param string $field 字段名
   * @return int
   */
  public function getFieldBindType(string $field): int
  {
    $fieldsType = $this->getFieldsBindType();
    return $fieldsType[$field];
  }
}
