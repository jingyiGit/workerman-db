<?php
declare (strict_types=1);

namespace DiscuzDb\db\concern;

/**
 * 事务支持
 */
trait Transaction
{
  /**
   * 执行数据库事务
   *
   * @access public
   * @param callable $callback 数据操作方法回调
   * @return mixed
   */
  public function transaction(callable $callback)
  {
    global $_GG;
    if ($_GG['__IN_TRANSACTION__'] > 0) {
      $_GG['__IN_TRANSACTION__']++;
    } else {
      $_GG['__IN_TRANSACTION__'] = 1;
      $this->startTrans();
    }
    
    $res = $callback();
    if (--$_GG['__IN_TRANSACTION__'] < 1) {
      $this->commit();
      unset($_GG['__IN_TRANSACTION__']);
    }
    return $res;
  }
  
  /**
   * 启动事务
   *
   * @access public
   * @return void
   */
  public function startTrans(): void
  {
    $this->discuz_startTrans();
  }
  
  /**
   * 用于非自动提交状态下面的查询提交
   *
   * @access public
   * @return void
   * @throws PDOException
   */
  public function commit(): void
  {
    $this->discuz_commit();
  }
  
  /**
   * 事务回滚
   *
   * @access public
   * @return void
   * @throws PDOException
   */
  public function rollback(): void
  {
    $this->discuz_rollback();
  }
  
  /**
   * 执行数据库Xa事务
   *
   * @access public
   * @param callable $callback 数据操作方法回调
   * @return mixed
   * @throws PDOException
   * @throws \Exception
   * @throws \Throwable
   */
  public function transactionXa(callable $callback)
  {
    return $this->discuz_transactionXa($callback);
  }
  
  /**
   * 启动XA事务
   *
   * @access public
   * @param string $xid XA事务id
   * @return void
   */
  public function startTransXa(string $xid): void
  {
    $this->discuz_startTransXa($xid);
  }
  
  /**
   * 预编译XA事务
   *
   * @access public
   * @param string $xid XA事务id
   * @return void
   */
  public function prepareXa(string $xid): void
  {
    $this->discuz_prepareXa($xid);
  }
  
  /**
   * 提交XA事务
   *
   * @access public
   * @param string $xid XA事务id
   * @return void
   */
  public function commitXa(string $xid): void
  {
    $this->discuz_commitXa($xid);
  }
  
  /**
   * 回滚XA事务
   *
   * @access public
   * @param string $xid XA事务id
   * @return void
   */
  public function rollbackXa(string $xid): void
  {
    $this->discuz_rollbackXa($xid);
  }
}
