<?php
define('_WWW_ROOT_', dirname(__DIR__, 4) . "/");
spl_autoload_register(function ($class) {
  if (strpos($class, 'Applications\\') !== false) {
    $file = str_replace('\\', '/', _WWW_ROOT_ .  $class . '.php');
    if (file_exists($file)) {
      include_once $file;
    }
  }
}, true);
